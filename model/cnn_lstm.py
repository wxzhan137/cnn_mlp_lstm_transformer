from numpy import load
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, AveragePooling2D, Reshape, UpSampling2D, TimeDistributed, LSTM
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot as plt

# Create model
cnnlstm = Sequential()

#add model layers
pre_range = 5
# First conv2d Output size (96, 256, 16)
cnnlstm.add(TimeDistributed(
    Conv2D(16, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 96, 256, 4),
           data_format="channels_last",
           padding="same")))
# Second conv2d Output size (96, 256, 16)
cnnlstm.add(TimeDistributed(
    Conv2D(16, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 96, 256, 16),
           padding="same")))
# First AveragePooling 2D Output size (48, 128, 16)
cnnlstm.add(TimeDistributed(
    AveragePooling2D(pool_size=(2, 2),
                     input_shape=(pre_range, 96, 256, 16),
                     strides=(2, 2))))
# Third cond2d Output size (48, 128, 8)
cnnlstm.add(TimeDistributed(
    Conv2D(8, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 48, 128, 16),
           padding="same")))
# Fourth cond2d Output size (48, 128, 8)
cnnlstm.add(TimeDistributed(
    Conv2D(8, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 48, 128, 8),
           padding="same")))
# Second AveragePooling 2d Output size (24, 64, 8)
cnnlstm.add(TimeDistributed(
    AveragePooling2D(pool_size=(2, 2),
                     input_shape=(pre_range, 48, 128, 8),
                     strides=(2, 2))))
# Fifth cond2d Output size (24, 64, 8)
cnnlstm.add(TimeDistributed(
    Conv2D(8, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 24, 64, 8),
           padding="same")))
# Sixth cond2d Output size (24, 64, 8)
cnnlstm.add(TimeDistributed(
    Conv2D(8, kernel_size=3,
           activation='tanh',
           input_shape=(pre_range, 24, 64, 8),
           padding="same")))
# Third AveragePooling 2d Output size (12, 32, 8)
cnnlstm.add(TimeDistributed(
    AveragePooling2D(pool_size=(2, 2),
                     input_shape=(pre_range, 24, 64, 8),
                     strides=(2, 2))))

# First reshape Output size (1, 3072)
cnnlstm.add(TimeDistributed(Flatten()))

cnnlstm.add(
    LSTM(3072,
         activation='relu',
         return_sequences=False))

# First MLP Output size (3072)
cnnlstm.add(Dense(3072, activation='tanh'))
# Second MLP Output size (3072)
cnnlstm.add(Dense(3072, activation='tanh'))
# Second reshape Output size (12, 32, 8)
cnnlstm.add(Reshape((12, 32, 8)))
# Seventh cond2d Output size (12, 32, 8)
cnnlstm.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Eighth cond2d Output size (12, 32, 8)
cnnlstm.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# First Upsampling 2d Output size (24, 64, 8)
cnnlstm.add(UpSampling2D(size=(2, 2)))
# Ninth cond2d Output size (24, 64, 8)
cnnlstm.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# 10th cond2d Output size (24, 64, 8)
cnnlstm.add(Conv2D(8, kernel_size=3, activation='tanh', padding="same"))
# Second Upsampling 2d Output size (48, 128, 8) ?? wrong in the paper ??
cnnlstm.add(UpSampling2D(size=(2, 2)))
# 11th cond2d Output size (48, 128, 16)
cnnlstm.add(Conv2D(16, kernel_size=3, activation='tanh', padding="same"))
# 12th cond2d Output size (48, 128, 16)
cnnlstm.add(Conv2D(16, kernel_size=3, activation='tanh', padding="same"))
# Third Upsampling 2d Output size (96, 256, 16)
cnnlstm.add(UpSampling2D(size=(2, 2)))
# 13th cond2d Output size (96, 256, 4)
cnnlstm.add(Conv2D(4, kernel_size=3, activation='tanh', padding="same"))

# Compile model using accuracy to measure model performance
cnnlstm.compile(optimizer="adam", loss="mean_absolute_error", metrics=['accuracy'])

# cnnlstm.build((None, pre_range, 96, 256, 4))
# cnnlstm.summary()
#summary load data
## normalize ??
#
from video_generator import SequenceGenerator

datagen = SequenceGenerator("/Users/wzhan/PycharmProjects/Tubulent_inflow/data/2000",
                            5,
                            4)
# data = load("/Users/wzhan/PycharmProjects/Tubulent_inflow/data/2000/data.npy")

# indices = np.random.permutation(data.shape[0])
# print("data shape", data.shape)
# training_idx, test_idx = indices[:int(0.8*data.shape[0])], indices[int(0.8*data.shape[0]):]
# print("indice", training_idx.shape, test_idx.shape)
# training_sect, test_sect = data[training_idx, :, :, :], data[test_idx, :, :, :]
# train_iterator = datagen.flow(training_sect, training_sect, batch_size=32, shuffle=True)
# # test_iterator = datagen.flow(test_sect, test_sect, batch_size=32, shufflefle=True)
history = cnnlstm.fit(datagen,
                      epochs=20,
                      batch_size=4,
                      steps_per_epoch=60)
# _, acc = CNNencoder.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
# print('Test Accuracy: %.3f' % (acc * 100))

# save model
model_json = cnnlstm.to_json()
with open("cnnencoder.json", "w") as json_file:
    json_file.write(model_json)

cnnlstm.save_weights('/Users/wzhan/PycharmProjects/Tubulent_inflow/cnn-autoencoder/cnnlstm.h5')
print("Saved the model")

print(history.history.keys())
# plotting
plt.plot(history.history['accuracy'])
# plt.plot(history.history['val_accuracy'])
plt.title("model accuracy")
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

plt.plot(history.history['loss'])
# plt.plot(history.historytory['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()





