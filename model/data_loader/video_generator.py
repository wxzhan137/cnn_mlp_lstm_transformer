# turbulent flow sequence generator
import os
import glob
import numpy as np
import pathlib
from numpy import load

from keras.utils import Sequence
from keras.preprocessing.image import ImageDataGenerator, img_to_array

class SequenceGenerator(Sequence):

    def __init__(self,
                 data_path,
                 nb_sequences: int=5,
                 batch_size: int = 4):

        # self.data = load(dataa_path)
        # self.nb_frames = self.data.shape[0]

# 0
#         if split_val is not None:
#             assert 0.0 < split_val < 1.0
#
#         if split_test is not None:
#             assert 0.0 < split_test < 1.0

        # split_val = split_val if split_val is not None else 0.0
        # split_test = split_test if split_test is not None else 0.0

        self.data_path = data_path
        self.nb_sequences = nb_sequences
        self.batch_size = batch_size
        self.nb_frames = 0
        self.data = []

        # if split_val > 0:
        #     indexs = np.arrange(len(self.nb_frames))
        #     nb_val = 0
        #
        #     nbval = int(split_val * (self.nb_frames - nb_sequences))
        #     nbtrain = (self.nb_frames - nb_sequences) - nbval
        #
        #     train_index = indexs[:nbtrain]
        #     val_index = indexs[nbtrain:]
        self.__filecount = 0
        self.__list_all_files()

    def __len__(self):
        print("get length")
        count = 0
        for root, dirs, files in os.walk(self.data_path):
            for file in files:
                if file.endswith('.npy'):
                    count += 1
        print((self.nb_frames-self.nb_sequences)//self.batch_size-2)
        return ((self.nb_frames-self.nb_sequences)//self.batch_size) -2

    def __list_all_files(self):
        print("list all files")
        i = 1
        print("Inject frames in memory, could take a while")
        for root, dirs, files in os.walk(self.data_path):
            for file in files:
                if file.endswith('.npy'):
                    self.__filecount += 1
                    self.__openframe(os.path.join(root, file))
                    i += 1

    def __openframe(self, file):
        print("open frame")
        data = load(file)
        nb_frames = data.shape[0]
        self.nb_frames += nb_frames
        step = nb_frames - self.nb_sequences - 2
        for i in range(step):
            x = data[i:i+self.nb_sequences, :, :, :]
            y = data[self.nb_sequences+i+1 + 1, :, :, :]
            self.data.append((x, y))
        print("self.data length", len(self.data))

    def on_epoch_end(self):
        print("on epoch end")
        """ When epoch has finished, random shuffle images in memory """
        pass

    def __getitem__(self, index):
        X = []
        Y = []
        for i in range(self.batch_size):
            print("index: ", index, index*self.batch_size+i)
            x, y = self.data[index*self.batch_size+i]
            X.append(x)
            Y.append(y)
        # for i in range(256):
        #     print("index", index)
        #     x, y = self.data[i]
        #     X.append(x)
        #     Y.append(y)

        return np.array(X), np.array(Y)


if __name__ == "__main__":
    obj = SequenceGenerator("/Users/wzhan/PycharmProjects/Tubulent_inflow/data/2000",
                            5,
                            1)
    print(obj.__len__())


