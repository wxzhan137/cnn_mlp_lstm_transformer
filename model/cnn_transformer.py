import math
import torch
import torch.nn as nn
import torch.nn.functional as F

from . import head_helper, resnet_helper, stem_helper, vtn_helper
from .build import MODEL_REGISTRY


# Define the Convolutional Autoencoder
class CNNencoder(nn.Module):
    def __init__(self):
        super(CNNencoder, self).__init__()

        # Encoder
        ## in out kernel
        # need to know the padding,
        self.encoder = nn.Sequential(
            nn.Conv2d(in_channels=4, out_channels=16, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(in_channels=16, out_channels=8, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0),
            nn.Tanh(True),
            nn.AvgPool2d(kernel_size=2, stride=2),
            nn.Flatten(),
            nn.Linear(in_features=3072, out_features=3072),
            nn.Tanh(True))


    def forward(self, x):
        x = self.encoder(x)
        return x

# Define the Convolutional Autoencoder
class CNNdecoder(nn.Module):
    def __init__(self):
        super(CNNdecoder, self).__init__()

        # decoder
        self.lin1 = nn.Linear(in_features=3072, out_features=3072)
        self.conv1 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0)  # need to know the padding
        self.conv2 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0)
        self.up1 = nn.Upsample(scale_factor=2, mode='nearest')
        self.conv3 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0)
        self.conv4 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, padding=0)
        self.up2 = nn.Upsample(scale_factor=2, mode='nearest')
        self.conv5 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, padding=0)
        self.conv6 = nn.Conv2d(in_channels=16, out_channels=16, kernel_size=3, padding=0)
        self.up3 = nn.Upsample(scale_factor=2, mode='nearest')
        self.conv7 = nn.Conv2d(in_channels=16, out_channels=4, kernel_size=3, padding=0)



    def forward(self, x):
        x = F.tanh(self.lin1(x))
        x = x.reshape(x, (12, 32, 8))
        x = F.tanh(self.conv1(x))
        x = F.tanh(self.conv2(x))
        x = self.up1(x)
        x = F.tanh(self.conv3(x))
        x = F.tanh(self.conv4(x))
        x = self.up2(x)
        x = F.tanh(self.conv5(x))
        x = F.tanh(self.conv6(x))
        x = self.up3(x)
        x = F.tanh(self.conv7(x))

        return x

@MODEL_REGISTRY.register()
class VTN(nn.Module):
    """
    VTN model builder. It uses ViT-Base as the backbone.

    Daniel Neimark, Omri Bar, Maya Zohar and Dotan Asselmann.
    "Video Transformer Network."
    https://arxiv.org/abs/2102.00719
    """

    def __init__(self, cfg):
        """
        The `__init__` method of any subclass should also contain these
            arguments.
        Args:
            cfg (CfgNode): model building configs, details are in the
                comments of the config file.
        """
        super(VTN, self).__init__()
        self._construct_network(cfg)

    def _construct_network(self, cfg):
        """
        Builds a VTN model, with a given backbone architecture.
        Args:
            cfg (CfgNode): model building configs, details are in the
                comments of the config file.
        """
        self.backbone = CNNencoder()

        embed_dim = self.backbone.embed_dim #?? What is this variable
        self.cls_token = nn.Parameter(torch.randn(1, 1, embed_dim))

        self.temporal_encoder = vtn_helper.VTNLongformerModel(
            embed_dim=embed_dim,
            max_position_embeddings=cfg.VTN.MAX_POSITION_EMBEDDINGS,
            num_attention_heads=cfg.VTN.NUM_ATTENTION_HEADS,
            num_hidden_layers=cfg.VTN.NUM_HIDDEN_LAYERS,
            attention_mode=cfg.VTN.ATTENTION_MODE,
            pad_token_id=cfg.VTN.PAD_TOKEN_ID,
            attention_window=cfg.VTN.ATTENTION_WINDOW,
            intermediate_size=cfg.VTN.INTERMEDIATE_SIZE,
            attention_probs_dropout_prob=cfg.VTN.ATTENTION_PROBS_DROPOUT_PROB,
            hidden_dropout_prob=cfg.VTN.HIDDEN_DROPOUT_PROB)

        self.decoder = CNNdecoder()

    def forward(self, x, bboxes=None):

        x, position_ids = x

        # spatial backbone
        B, C, F, H, W = x.shape
        x = x.permute(0, 2, 1, 3, 4)
        x = x.reshape(B * F, C, H, W)
        x = self.backbone.forward(x)
        x = x.reshape(B, F, -1)

        # temporal encoder (Longformer)
        B, D, E = x.shape
        attention_mask = torch.ones((B, D), dtype=torch.long, device=x.device)
        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        x = torch.cat((cls_tokens, x), dim=1)
        cls_atten = torch.ones(1).expand(B, -1).to(x.device)
        attention_mask = torch.cat((attention_mask, cls_atten), dim=1)
        attention_mask[:, 0] = 2
        x, attention_mask, position_ids = vtn_helper.pad_to_window_size_local(
            x,
            attention_mask,
            position_ids,
            self.temporal_encoder.config.attention_window[0],
            self.temporal_encoder.config.pad_token_id)
        token_type_ids = torch.zeros(x.size()[:-1], dtype=torch.long, device=x.device)
        token_type_ids[:, 0] = 1

        # position_ids
        position_ids = position_ids.long()
        mask = attention_mask.ne(0).int()
        max_position_embeddings = self.temporal_encoder.config.max_position_embeddings
        position_ids = position_ids % (max_position_embeddings - 2)
        position_ids[:, 0] = max_position_embeddings - 2
        position_ids[mask == 0] = max_position_embeddings - 1

        x = self.temporal_encoder(input_ids=None,
                                  attention_mask=attention_mask,
                                  token_type_ids=token_type_ids,
                                  position_ids=position_ids,
                                  inputs_embeds=x,
                                  output_attentions=None,
                                  output_hidden_states=None,
                                  return_dict=None)
        # MLP head
        x = x["last_hidden_state"]
        x = self.decoder.forward(x[:, 0])
        return x